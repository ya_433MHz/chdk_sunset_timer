#include <JeeLib.h> // Low power functions library
ISR(WDT_vect) { 
  Sleepy::watchdogEvent(); 
} // Setup the watchdog
// Include the low power lib from RocketScream 
// http://www.rocketscream.com/blog/2011/07/04/lightweight-low-power-arduino-library/
// #include "LowPower.h" // 

// RTC Related stuff.Uses DS3231 lib from http://forum.arduino.cc/index.php/topic,57642.0.html
// Module used in testing was similar to this http://www.ebay.com/itm/DS3231-AT24C32-IIC-Precision-RTC-Real-Time-Clock-Memory-Module-For-Arduino-/141135165489?pt=LH_DefaultDomain_0&hash=item20dc4fb831

#include <DS3231.h>
// We also need the Wire library for i2c for the RTC (and for the 24CXX on the RTC board to store our schedule).
#include <Wire.h>
#include "RTClib.h"  // Credit: Adafruit
RTC_DS1307 RTC;

// Include the Software PWM library, this is used so that we can define any pin for the backlight, rather than just one of the hardware PWM pins.
#include <SoftPWM.h>

//
# define MOSFET1_PWR 14
# define MOSFET2_PWR 15


// Set up the RTC
DS3231 Clock;
# define RTC_PWR 9

static byte NowSecond = 0;
static byte NowMinute = 0;
static byte NowHour = 0;
static byte NowDoW = 0;
static byte NowDate = 0;
static byte NowMonth = 0;
static byte NowYear = 0;

// Sunrise and Sunset library. 
// More details here. http://www.andregoncalves.info/ag_blog/?p=47

#include <Sunrise.h>
// create a Sunrise object
//Examples: Lisbon, Portugal, Europe - Latitude/Longitude and Timezone 	38.79/-9.12, 0
//          Stirling, Scotlans 56.1172° N, 3.9397° W - GMT/BST
static float MyLatitude = 56.126366;
static float MyLongitude = -4.2353026;
static float MyUTCOffset = 1;
Sunrise mySunrise(MyLatitude,MyLongitude,MyUTCOffset);


/*
NOTE: Philips/Nokia 5110 LCD Scrolling text example code
 Modified from: http://www.arduino.cc/playground/Code/PCD8544
 */

// Nokia 5110 stuff.. #defines control all of the pins of a display like this... 
// http://www.ebay.com/itm/2PCS-Nokia-5110-LCD-Module-Blue-Backlight-84X48-adapter-PCB-/400498108300?pt=LH_DefaultDomain_0&hash=item5d3f8c278c
//
// Define the display pins to use on the arduino - the exact order will depend on your display - also take care that the display can tolerate the voltage
// of the atmel output(s). My sample of the version linked above is happy with +5V for the display and +5V PWM for the backlight, so I connected it directly. 
// YMMV, you may need limit resistors in line for whatever display you chose to use, read the instructions with the display you purchase. 

// The display I used allows 
// Backlight (software PWM)
#define BACK_LIGHT 2

// Using  GPIO pins for everything including power pin, allows us to completely turn off the LCD display
#define DISP_PWR  3
// Program pins
#define PIN_SCLK  4
#define PIN_SDIN  5
#define PIN_DC    6
#define PIN_RESET 8
#define PIN_SCE   7

// Define the configuration for the LCD 
#define LCD_C     LOW
#define LCD_D     HIGH
#define LCD_CMD   0

// Size of the LCD
#define LCD_X     84
#define LCD_Y     48

// Reserve 18 characters for text line buffer
static char MessageBuffer[18];

// Default temperature
static long temperature = 20;

// Default Scroll Position
int scrollPosition = -10;

#include <string.h>
#include<stdlib.h>

// Character font array - font is 8 buts high, 5 bits wide 
// Effectively this allows for a maximum of 6 lines of 16 chars (actually 16.8 characters) 

static const byte ASCII[][5] =
{
  {
    0x00, 0x00, 0x00, 0x00, 0x00            } // 20
  ,{
    0x00, 0x00, 0x5f, 0x00, 0x00            } // 21 !
  ,{
    0x00, 0x07, 0x00, 0x07, 0x00            } // 22 "
  ,{
    0x14, 0x7f, 0x14, 0x7f, 0x14            } // 23 #
  ,{
    0x24, 0x2a, 0x7f, 0x2a, 0x12            } // 24 $
  ,{
    0x23, 0x13, 0x08, 0x64, 0x62            } // 25 %
  ,{
    0x36, 0x49, 0x55, 0x22, 0x50            } // 26 &
  ,{
    0x00, 0x05, 0x03, 0x00, 0x00            } // 27 '
  ,{
    0x00, 0x1c, 0x22, 0x41, 0x00            } // 28 (
  ,{
    0x00, 0x41, 0x22, 0x1c, 0x00            } // 29 )
  ,{
    0x14, 0x08, 0x3e, 0x08, 0x14            } // 2a *
  ,{
    0x08, 0x08, 0x3e, 0x08, 0x08            } // 2b +
  ,{
    0x00, 0x50, 0x30, 0x00, 0x00            } // 2c ,
  ,{
    0x08, 0x08, 0x08, 0x08, 0x08            } // 2d -
  ,{
    0x00, 0x60, 0x60, 0x00, 0x00            } // 2e .
  ,{
    0x20, 0x10, 0x08, 0x04, 0x02            } // 2f /
  ,{
    0x3e, 0x51, 0x49, 0x45, 0x3e            } // 30 0
  ,{
    0x00, 0x42, 0x7f, 0x40, 0x00            } // 31 1
  ,{
    0x42, 0x61, 0x51, 0x49, 0x46            } // 32 2
  ,{
    0x21, 0x41, 0x45, 0x4b, 0x31            } // 33 3
  ,{
    0x18, 0x14, 0x12, 0x7f, 0x10            } // 34 4
  ,{
    0x27, 0x45, 0x45, 0x45, 0x39            } // 35 5
  ,{
    0x3c, 0x4a, 0x49, 0x49, 0x30            } // 36 6
  ,{
    0x01, 0x71, 0x09, 0x05, 0x03            } // 37 7
  ,{
    0x36, 0x49, 0x49, 0x49, 0x36            } // 38 8
  ,{
    0x06, 0x49, 0x49, 0x29, 0x1e            } // 39 9
  ,{
    0x00, 0x36, 0x36, 0x00, 0x00            } // 3a :
  ,{
    0x00, 0x56, 0x36, 0x00, 0x00            } // 3b ;
  ,{
    0x08, 0x14, 0x22, 0x41, 0x00            } // 3c <
  ,{
    0x14, 0x14, 0x14, 0x14, 0x14            } // 3d =
  ,{
    0x00, 0x41, 0x22, 0x14, 0x08            } // 3e >
  ,{
    0x02, 0x01, 0x51, 0x09, 0x06            } // 3f ?
  ,{
    0x32, 0x49, 0x79, 0x41, 0x3e            } // 40 @
  ,{
    0x7e, 0x11, 0x11, 0x11, 0x7e            } // 41 A
  ,{
    0x7f, 0x49, 0x49, 0x49, 0x36            } // 42 B
  ,{
    0x3e, 0x41, 0x41, 0x41, 0x22            } // 43 C
  ,{
    0x7f, 0x41, 0x41, 0x22, 0x1c            } // 44 D
  ,{
    0x7f, 0x49, 0x49, 0x49, 0x41            } // 45 E
  ,{
    0x7f, 0x09, 0x09, 0x09, 0x01            } // 46 F
  ,{
    0x3e, 0x41, 0x49, 0x49, 0x7a            } // 47 G
  ,{
    0x7f, 0x08, 0x08, 0x08, 0x7f            } // 48 H
  ,{
    0x00, 0x41, 0x7f, 0x41, 0x00            } // 49 I
  ,{
    0x20, 0x40, 0x41, 0x3f, 0x01            } // 4a J
  ,{
    0x7f, 0x08, 0x14, 0x22, 0x41            } // 4b K
  ,{
    0x7f, 0x40, 0x40, 0x40, 0x40            } // 4c L
  ,{
    0x7f, 0x02, 0x0c, 0x02, 0x7f            } // 4d M
  ,{
    0x7f, 0x04, 0x08, 0x10, 0x7f            } // 4e N
  ,{
    0x3e, 0x41, 0x41, 0x41, 0x3e            } // 4f O
  ,{
    0x7f, 0x09, 0x09, 0x09, 0x06            } // 50 P
  ,{
    0x3e, 0x41, 0x51, 0x21, 0x5e            } // 51 Q
  ,{
    0x7f, 0x09, 0x19, 0x29, 0x46            } // 52 R
  ,{
    0x46, 0x49, 0x49, 0x49, 0x31            } // 53 S
  ,{
    0x01, 0x01, 0x7f, 0x01, 0x01            } // 54 T
  ,{
    0x3f, 0x40, 0x40, 0x40, 0x3f            } // 55 U
  ,{
    0x1f, 0x20, 0x40, 0x20, 0x1f            } // 56 V
  ,{
    0x3f, 0x40, 0x38, 0x40, 0x3f            } // 57 W
  ,{
    0x63, 0x14, 0x08, 0x14, 0x63            } // 58 X
  ,{
    0x07, 0x08, 0x70, 0x08, 0x07            } // 59 Y
  ,{
    0x61, 0x51, 0x49, 0x45, 0x43            } // 5a Z
  ,{
    0x00, 0x7f, 0x41, 0x41, 0x00            } // 5b [
  ,{
    0x00, 0x06, 0x09, 0x09, 0x06            } // 5c (degrees)
  ,{
    0x00, 0x41, 0x41, 0x7f, 0x00            } // 5d ]
  ,{
    0x04, 0x02, 0x01, 0x02, 0x04            } // 5e ^
  ,{
    0x40, 0x40, 0x40, 0x40, 0x40            } // 5f _
  ,{
    0x00, 0x01, 0x02, 0x04, 0x00            } // 60 `
  ,{
    0x20, 0x54, 0x54, 0x54, 0x78            } // 61 a
  ,{
    0x7f, 0x48, 0x44, 0x44, 0x38            } // 62 b
  ,{
    0x38, 0x44, 0x44, 0x44, 0x20            } // 63 c
  ,{
    0x38, 0x44, 0x44, 0x48, 0x7f            } // 64 d
  ,{
    0x38, 0x54, 0x54, 0x54, 0x18            } // 65 e
  ,{
    0x08, 0x7e, 0x09, 0x01, 0x02            } // 66 f
  ,{
    0x0c, 0x52, 0x52, 0x52, 0x3e            } // 67 g
  ,{
    0x7f, 0x08, 0x04, 0x04, 0x78            } // 68 h
  ,{
    0x00, 0x44, 0x7d, 0x40, 0x00            } // 69 i
  ,{
    0x20, 0x40, 0x44, 0x3d, 0x00            } // 6a j
  ,{
    0x7f, 0x10, 0x28, 0x44, 0x00            } // 6b k
  ,{
    0x00, 0x41, 0x7f, 0x40, 0x00            } // 6c l
  ,{
    0x7c, 0x04, 0x18, 0x04, 0x78            } // 6d m
  ,{
    0x7c, 0x08, 0x04, 0x04, 0x78            } // 6e n
  ,{
    0x38, 0x44, 0x44, 0x44, 0x38            } // 6f o
  ,{
    0x7c, 0x14, 0x14, 0x14, 0x08            } // 70 p
  ,{
    0x08, 0x14, 0x14, 0x18, 0x7c            } // 71 q
  ,{
    0x7c, 0x08, 0x04, 0x04, 0x08            } // 72 r
  ,{
    0x48, 0x54, 0x54, 0x54, 0x20            } // 73 s
  ,{
    0x04, 0x3f, 0x44, 0x40, 0x20            } // 74 t
  ,{
    0x3c, 0x40, 0x40, 0x20, 0x7c            } // 75 u
  ,{
    0x1c, 0x20, 0x40, 0x20, 0x1c            } // 76 v
  ,{
    0x3c, 0x40, 0x30, 0x40, 0x3c            } // 77 w
  ,{
    0x44, 0x28, 0x10, 0x28, 0x44            } // 78 x
  ,{
    0x0c, 0x50, 0x50, 0x50, 0x3c            } // 79 y
  ,{
    0x44, 0x64, 0x54, 0x4c, 0x44            } // 7a z
  ,{
    0x08, 0x04, 0x7e, 0x04, 0x08           } // 7b (Up Arrow)
  ,{
    0x7f, 0x26, 0x63, 0x22, 0x7f            } // 7c (Chip)
  ,{
    0x10, 0x20, 0x7e, 0x20, 0x10           } // 7d  (Down Arrow)
  ,{
    0x10, 0x08, 0x08, 0x10, 0x08            } // 7e ←
  ,{
    0x00, 0x06, 0x09, 0x09, 0x06            } // 7f →
};

void LcdCharacter(char character)
{
  LcdWrite(LCD_D, 0x00);
  for (int index = 0; index < 5; index++)
  {
    LcdWrite(LCD_D, ASCII[character - 0x20][index]);
  }
  LcdWrite(LCD_D, 0x00);
}

void LcdClear(void)
{
  for (int index = 0; index < LCD_X * LCD_Y / 8; index++)
  {
    LcdWrite(LCD_D, 0x00);
  }
}

void Mosfet1Poweron(void)
// Note sense is reversed LOW switches on, HIGH, off
{
  pinMode(MOSFET1_PWR, OUTPUT);
  digitalWrite(MOSFET1_PWR, LOW);
}

void Mosfet2Poweron(void)
{
  pinMode(MOSFET2_PWR, OUTPUT);
  digitalWrite(MOSFET2_PWR, LOW);
}


void Mosfet1Poweroff(void)
{
  pinMode(MOSFET1_PWR, OUTPUT);
  digitalWrite(MOSFET1_PWR, HIGH);
}

void Mosfet2Poweroff(void)
{
  pinMode(MOSFET2_PWR, OUTPUT);
  digitalWrite(MOSFET2_PWR, HIGH);
}


void LcdPoweron(void)
{
  pinMode(RTC_PWR,   OUTPUT);
  digitalWrite(DISP_PWR, HIGH);
  // LcdInitialise();
  // LcdClear();
  // Draw a box on the Nokia display which we will overlay with *stuff*
  // drawBox(); 

}

void BacklightOn(){
  digitalWrite(BACK_LIGHT, LOW);
  SoftPWMSetPercent(BACK_LIGHT, 1);
}  

void BacklightOff(){
  digitalWrite(BACK_LIGHT, HIGH);
}  

void LcdPoweroff(void)
{
  LcdWrite(LCD_CMD, 0x25);  // LCD Power Down mode..
  // Set all pins as inputs - this effectively switches the display completely off. 
  pinMode(PIN_SCE,   INPUT);
  pinMode(PIN_RESET, INPUT);
  pinMode(PIN_DC,    INPUT);
  pinMode(PIN_SDIN,  INPUT);
  pinMode(PIN_SCLK,  INPUT);
  pinMode(DISP_PWR,  INPUT);
}


void LcdInverse() {

  LcdWrite(LCD_CMD, 0x21);  // LCD Extended Commands.
  LcdWrite(LCD_CMD, 0xB1);  // Set LCD Vop (Contrast). //B1
  LcdWrite(LCD_CMD, 0x04);  // Set Temp coefficent. //0x04
  LcdWrite(LCD_CMD, 0x14);  // LCD bias mode 1:48. //0x13
  LcdWrite(LCD_CMD, 0x0D);  // LCD in normal mode. 0x0d for inverse
  LcdWrite(LCD_C, 0x20);
  LcdWrite(LCD_C, 0x0C);
}

void LcdContrast(byte contrast) {

  LcdWrite(LCD_CMD, 0x21);  // LCD Extended Commands.
  LcdWrite(LCD_CMD, contrast);  // Set LCD Vop (Contrast). //B1
  LcdWrite(LCD_CMD, 0x04);  // Set Temp coefficent. //0x04
  LcdWrite(LCD_CMD, 0x14);  // LCD bias mode 1:48. //0x13
  //LcdWrite(LCD_CMD, 0x0C);  // LCD in normal mode. 0x0d for inverse
  LcdWrite(LCD_C, 0x20);
  LcdWrite(LCD_C, 0x0C);
}

void LcdBias(byte bias) {

  LcdWrite(LCD_CMD, 0x21);  // LCD Extended Commands.
  LcdWrite(LCD_CMD, 0xB1);  // Set LCD Vop (Contrast). //B1
  LcdWrite(LCD_CMD, 0x04);  // Set Temp coefficent. //0x04
  LcdWrite(LCD_CMD, bias);  // LCD bias mode 1:48. //0x13
  //LcdWrite(LCD_CMD, 0x0C);  // LCD in normal mode. 0x0d for inverse
  LcdWrite(LCD_C, 0x20);
  LcdWrite(LCD_C, 0x0C);
}

void LcdCycleContrast(){
  // Contrast values lie between 0xB1 (very light) and 0xBE (very dark) 
  for (byte LCDContrast = 0xB0; LCDContrast > 0xBE; LCDContrast++) {  
    LcdContrast(LCDContrast);
    // Show backlight level
    sprintf(MessageBuffer, " %02d ",LCDContrast); 
    gotoXY(1,4);
    LcdString(MessageBuffer);
    delay(200);
  }
  for (byte LCDContrast = 0xBE; LCDContrast > 0xB1; LCDContrast--) {  
    LcdContrast(LCDContrast);
    // Show backlight level
    sprintf(MessageBuffer, " %02d ",LCDContrast); 
    gotoXY(1,4);
    LcdString(MessageBuffer);
    delay(200);
  }
}

void LcdCycleBias(){
  for (byte LCDBias = 0x10; LCDBias < 0x18; LCDBias++) {  
    LcdBias(LCDBias);
    delay(500);
  }
}

void LcdInitialise(void)
{

  pinMode(PIN_SCE,   OUTPUT);
  pinMode(PIN_RESET, OUTPUT);
  pinMode(PIN_DC,    OUTPUT);
  pinMode(PIN_SDIN,  OUTPUT);
  pinMode(PIN_SCLK,  OUTPUT);
  pinMode(DISP_PWR,  OUTPUT);


  digitalWrite(PIN_RESET, LOW);
  digitalWrite(PIN_RESET, HIGH);

  digitalWrite(DISP_PWR, HIGH);

  LcdWrite(LCD_CMD, 0x21);  // LCD Extended Commands.
  LcdWrite(LCD_CMD, 0xB1);  // Set LCD Vop (Contrast). //B1
  LcdWrite(LCD_CMD, 0x04);  // Set Temp coefficent. //0x04
  LcdWrite(LCD_CMD, 0x14);  // LCD bias mode 1:48. //0x13
  LcdWrite(LCD_CMD, 0x0C);  // LCD in normal mode. 0x0d for inverse
  LcdWrite(LCD_C, 0x20);
  LcdWrite(LCD_C, 0x0C);
}

void LcdString(char *characters)
{
  while (*characters)
  {
    LcdCharacter(*characters++);
  }
}

void LcdWrite(byte dc, byte data)
{
  digitalWrite(PIN_DC, dc);
  digitalWrite(PIN_SCE, LOW);
  shiftOut(PIN_SDIN, PIN_SCLK, MSBFIRST, data);
  digitalWrite(PIN_SCE, HIGH);
}

/**
 * gotoXY routine to position cursor
 * x - range: 0 to 84
 * y - range: 0 to 5
 */
void gotoXY(int x, int y)
{
  LcdWrite( 0, 0x80 | x);  // Column.
  LcdWrite( 0, 0x40 | y);  // Row.
}

void drawBox(void)
{
  int j;
  for(j = 0; j < 84; j++) // top
  {
    gotoXY(j, 0);
    LcdWrite(1, 0x01);
  }

  for(j = 0; j < 84; j++) //Bottom
  {
    gotoXY(j, 5);
    LcdWrite(1, 0x80);
  }

  for(j = 0; j < 6; j++) // Right
  {
    gotoXY(83, j);
    LcdWrite(1, 0xff);
  }

  for(j = 0; j < 6; j++) // Left
  {
    gotoXY(0, j);
    LcdWrite(1, 0xff);
  }
}

void Scroll(String message)
{
  for (int i = scrollPosition; i < scrollPosition + 11; i++)
  {
    if ((i >= message.length()) || (i < 0))
    {
      LcdCharacter(' ');
    }
    else
    {
      LcdCharacter(message.charAt(i));
    }
  }
  scrollPosition++;
  if ((scrollPosition >= message.length()) && (scrollPosition > 0))
  {
    scrollPosition = -10;
  }
}

// Read Atmega328P internal temperature sensor //
long read_temp()
{
  // Read temperature sensor against 1.1V reference
  ADMUX = _BV(REFS1) | _BV(REFS0) | _BV(MUX3);
  // Start AD conversion
  ADCSRA |= _BV(ADEN) | _BV(ADSC);
  // Detect end-of-conversion
  while (bit_is_set(ADCSRA,ADSC));
  // return raw data
  return ADCL | (ADCH << 8);
}


// Alternative Internal Temp read. 
long readTemp()
{
  long result; // Read temperature sensor against 1.1V reference
  ADMUX = _BV(REFS1) | _BV(REFS0) | _BV(MUX3);
  delay(20); // Wait for Vref to settle - 2 was inadequate
  ADCSRA |= _BV(ADSC); // Convert
  while (bit_is_set(ADCSRA,ADSC));
  result = ADCL;
  result |= ADCH<<8;
  result = (result - 125) * 1075; //Don't forget to change "x" with multiplication sign
  return result;
} 


// Convert raw temperature data to °C
float conv_temp(long raw_temp)
{
  // f(x) = (raw - offset) / coeff
  return((raw_temp - 321.31) / 1.22);
}


// Read Atmega328P Supply voltage
const long scaleConst = 1184.665 * 1000;                // internalRef * 1023 * 1000;
float readVcc() {
  // Read 1.1V reference against AVcc
  // set the reference to Vcc and the measurement to the internal 1.1V reference
  ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  delay(2);                                                     // Wait for Vref to settle
  ADCSRA |= _BV(ADSC);                          // Start conversion
  while (bit_is_set(ADCSRA,ADSC));      // measuring
  uint8_t low  = ADCL;                          // must read ADCL first - it then locks ADCH 
  uint8_t high = ADCH;                          // unlocks both
  long result = (high<<8) | low;
  result = scaleConst / result;         // Calculate Vcc (in mV); 1125300 = 1.1*1023*1000
  return (float)result/1000;                           // Vcc in millivolts
  Mosfet1Poweroff();
  Mosfet2Poweroff();

}

void setup(void)
{
  
  // Start power and i2c for RTC 
  // NOTE: If you attempt to set up the LCD first, before powering on the RTC this will cause the system to hang
  // not sure why...

  // Power on the RTC
  pinMode(RTC_PWR,   OUTPUT);
  digitalWrite(RTC_PWR, HIGH);

  Wire.begin();
  RTC.begin();
  // FIXME: Uncomment the line below to set the RTC from the host.
  // I need a better mechanism for this. 
  //RTC.adjust(DateTime(__DATE__, __TIME__));
  // DateTime now = RTC.now();
  // LCD is all software (not i2c) 
  LcdInitialise();
  LcdClear();

  // This section grabs the current datetime and compares it to
  // the compilation time.  If necessary, the RTC is updated.
  DateTime now = RTC.now();
  DateTime compiled = DateTime(__DATE__, __TIME__);
  if (now.unixtime() < compiled.unixtime()) {
    //Serial.println("RTC is older than compile time! Updating");
    RTC.adjust(DateTime(__DATE__, __TIME__)); 
  }

  // Power off the RTC
  digitalWrite(RTC_PWR, LOW);

  // Take first Atmel temperature reading which we discard, to ensure we get a stable 2nd reading
  float cpu_temp=conv_temp(read_temp());
  drawBox(); 
}

void loop(void)
{
  //LCD On
  LcdPoweron();
  //Backlight set
  SoftPWMBegin(SOFTPWM_INVERTED);
  //digitalWrite(BACK_LIGHT, HIGH);
  SoftPWMSetPercent(BACK_LIGHT, 1);
  delay(1000);

  // AVR Supply voltage
  float supply_vcc=readVcc();
  int v1 = (supply_vcc - (int)supply_vcc) *100;
  sprintf(MessageBuffer,"Vcc %d.%dV ",(int)supply_vcc,v1);

  // Show the supply voltage
  gotoXY(7,5);
  LcdString(MessageBuffer);

  // Power on the RTC
  digitalWrite(RTC_PWR, HIGH);
  // Time and Temperature from DS323x 
  DateTime now = RTC.now();  
  float clock_temp=Clock.getTemperature();
  // Power off the RTC
  digitalWrite(RTC_PWR, LOW);
  int temp2 = (clock_temp - (int)clock_temp) * 10;

  // Power on the Display
  pinMode(DISP_PWR,  OUTPUT);
  digitalWrite(DISP_PWR, HIGH);
  // Show the time
  //  sprintf(MessageBuffer, "%02d:%02d:%02d ",now.hour(),now.minute(),now.second()); 
  sprintf(MessageBuffer, "  %02d:%02d  ",now.hour(),now.minute()); 
  gotoXY(11,0);
  LcdString(MessageBuffer);

  // Show the date

  sprintf(MessageBuffer, "%02d-%02d-%04d ",now.day(),now.month(),now.year()); 
  gotoXY(5,1);
  LcdString(MessageBuffer);

  // Show the RTC Temp
  sprintf(MessageBuffer," % 3d.%d\\C ",(int)clock_temp,temp2);
  gotoXY(13,4);
  LcdString(MessageBuffer);


  // Sunrise and sunset times
  byte h,m;
  int t;
  // t= minutes past midnight of sunrise (6 am would be 360)
  t=mySunrise.Rise(now.month(),now.day()); // (month,day) - january=1
  h=mySunrise.Hour();
  m=mySunrise.Minute();

  sprintf(MessageBuffer,"Sun{ %02d:%02d ",h,m);
  gotoXY(5,2);
  LcdString(MessageBuffer);

  // t= minutes past midnight of sunrise (6 am would be 360)
  t=mySunrise.Set(now.month(),now.day()); // (month,day) - january=1
  h=mySunrise.Hour();
  m=mySunrise.Minute();

  sprintf(MessageBuffer,"Sun} %02d:%02d ",h,m);
  gotoXY(5,3);
  LcdString(MessageBuffer);
  // Cycle backlight levels 
  for (int thisBacklight = 2; thisBacklight < 42; thisBacklight+=8) {  
    SoftPWMSetPercent(BACK_LIGHT, thisBacklight);
    // Show backlight level
    sprintf(MessageBuffer, " %02d ",thisBacklight); 
    gotoXY(1,1);
    LcdString(MessageBuffer);
    delay(100);
  }
  // LcdInverse();
  for (int thisBacklight = 42; thisBacklight > 2; thisBacklight-=8) {  
    // Show backlight level
    sprintf(MessageBuffer, " %02d ",thisBacklight); 
    gotoXY(1,1);
    LcdString(MessageBuffer);
    LcdCycleContrast();
    //LcdCycleBias();
    SoftPWMSetPercent(BACK_LIGHT, thisBacklight);
    delay(100);
  }
  Mosfet1Poweron();
  Mosfet2Poweron();
  delay(1000);
  BacklightOff();

  Sleepy::loseSomeTime(5000);
  // Power off the DisplayO
  LcdPoweroff();
  Sleepy::loseSomeTime(5000);

  //Sleepy::loseSomeTime(5000);
  BacklightOn();
  //  delay(1000);
  LcdPoweron();
  delay(1000);
  Mosfet1Poweroff();
  Mosfet2Poweroff();
  LcdInitialise();
  delay(1000);
  //Sleepy::loseSomeTime(5000);
  LcdClear();
  // LcdPoweroff();
  delay(1000);
  drawBox();

}






